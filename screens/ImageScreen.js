import React, {Component} from 'react';
import Title from '../components/header/Title';
import {View, Image, ActivityIndicator, StyleSheet} from 'react-native';

// import Icon from 'react-native-vector-icons/FontAwesome';


class ImageScreen extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      headerTitle: () => <Title title="Image preview" />
    };
  };

  render() {
    const {navigation} = this.props;
    const {imageUrl} = navigation.state.params;

    if (!imageUrl)
      return (
        <View style={[styles.container, styles.horizontal]}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      );
    return (
      <View>
        <Image
          style={{width: 400, height: 700}}
          source={{
            uri: `data:image/png;base64,${imageUrl.base64}`,
          }}
        />
      </View>
    );
  }
}

export default ImageScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
});
