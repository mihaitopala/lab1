import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  StatusBar,
} from 'react-native';

import SearchInGoogle from '../components/SearchInGoogle';
import ControlCamera from '../components/ControlCamera';
import SendPushNotification from '../components/SendPushNotification';
import Title from '../components/header/Title';

class HomeScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: () => <Title title="Home" />,
    };
  };

  render() {
    const { navigation } = this.props;

    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView style={styles.sectionContainer}>
          <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            style={styles.scrollView}>
                <SearchInGoogle />
                <ControlCamera navigation={navigation} />
                <SendPushNotification />
          </ScrollView>
        </SafeAreaView>
      </>
    )
  }
};

const styles = StyleSheet.create({
  scrollView: {
    marginHorizontal: 0,
  },
  sectionContainer: {
    flex: 1,
    justifyContent: 'center',
    marginTop: 15
  },
});

export default HomeScreen;
