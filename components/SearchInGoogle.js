import React, {useState} from 'react';

import {
  View,
  TextInput,
  Text,
  TouchableOpacity,
  Linking,
  StyleSheet,
} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';

const SearchInGoogle = () => {
  const [searchText, setSearchText] = useState('');

  handleClick = () => {
    if (searchText.length) {
      return Linking.openURL(`https://google.com/search?q=${searchText}`);
    }
  };

  return (
    <View>
      <View>
        <Text style={styles.title}>Search in Google</Text>
      </View>
      <View style={styles.container}>
        <TextInput
          style={styles.input}
          value={searchText}
          onChangeText={text => setSearchText(text)}
          placeholder="Enter text..."
        />
        <TouchableOpacity style={styles.button} onPress={handleClick}>
          <Text style={styles.text}> Search </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default SearchInGoogle;

const styles = StyleSheet.create({
  title: {
    textAlign: 'center',
    marginBottom: 15,
    fontFamily: 'Roboto-Medium',
    fontSize: 18
  },
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 30,
    paddingBottom: 30
  },
  text: {
    fontSize: 14,
    fontWeight: 'bold',
    paddingBottom: 5,
    color: '#fff'
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#4630EB',
    color: Colors.white,
    padding: 10,
    borderRadius: 24,
    marginLeft: 10
  },
  input: {
    height: 40,
    borderColor: '#dfe1e5',
    borderWidth: 1,
    width: 230,
    borderRadius: 10
  },
});
