import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import {Colors} from 'react-native/Libraries/NewAppScreen';

import RadioButtons from './common/RadioButtons';

const options = [
  {
    text: 'Front',
    key: 'front',
  },
  {
    text: 'Back',
    key: 'back',
  },
];

const ControlCamera = ({navigation}) => {
  const [value, setValue] = useState('front');
  const [loading, setLoading ] = useState(false);
  const [showCamera, setShowCamera] = useState(false);
  camera = null;

  takePicture = async () => {
    setLoading(true);

    if (camera) {
      const options = {quality: 0.5, base64: true};
      const data = await camera.takePictureAsync(options);
      setShowCamera(false);
      setLoading(false)
      return navigation.navigate('Image', {imageUrl: data});
    }
  };

  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.title}>Control your camera</Text>
      </View>
      <RadioButtons options={options} value={value} handleChange={setValue} />
      {showCamera ? (
        <View>
          <RNCamera
            ref={ref => {
              camera = ref;
            }}
            captureAudio={false}
            style={styles.preview}
            type={RNCamera.Constants.Type[value]}
          />
          <TouchableOpacity style={styles.button} onPress={takePicture}>
            <Text style={styles.smallText}>Take a picture</Text>
          </TouchableOpacity>
        </View>
      ) : (
        <TouchableOpacity
          style={styles.button}
          onPress={() => setShowCamera(!showCamera)}>
          <Text style={styles.smallText}>Open camera</Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

export default ControlCamera;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#dfe1e5',
    marginTop: 20,
    paddingTop: 30,
    paddingBottom: 30
  },
  title: {
    textAlign: 'center',
    marginBottom: 15,
    fontFamily: 'Roboto-Medium',
    fontSize: 18,
  },
  text: {
    fontSize: 18,
    fontWeight: 'bold',
    paddingBottom: 5,
    paddingTop: 30,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#4630EB',
    color: Colors.white,
    padding: 10,
    width: 150,
    borderRadius: 5,
    marginLeft: 105
  },
  smallText: {
    fontSize: 16,
    textShadowColor: '#fff',
    color: '#fff',
  },
  preview: {
    marginTop: 20,
    marginBottom: 50,
    alignItems: 'center',
    height: 400,
    width: Dimensions.get('window').width,
  },
  capture: {
    backgroundColor: '#fff',
    borderRadius: 5,
    color: '#000',
    padding: 10,
    margin: 40,
  },
  camera: {
    flex: 1,
    justifyContent: 'center',
  },
});
