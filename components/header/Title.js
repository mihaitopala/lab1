import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';

const Title = ({ title }) => {
    return (
        <View>
            <Text style={styles.text}>{title}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    text: {
        fontSize: 16,
        fontFamily: 'Roboto-Bold'
    }
});

export default Title;