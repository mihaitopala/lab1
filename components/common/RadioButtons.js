import React from 'react';

import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';

const RadioButtons = ({ options, value, handleChange }) => {
  return (
    <View style={styles.wrapper}>
      {options.map(item => {
        return (
          <View key={item.key} style={styles.buttonContainer}>
            <Text>{item.text}</Text>
            <TouchableOpacity 
              style={styles.circle}
              onPress={() => handleChange(item.key)} 
            >
                { value === item.key && (<View style={styles.checkedCircle} />) }
          </TouchableOpacity>
          </View>
        )
      })}
    </View>
  );
};

export default RadioButtons;

const styles = StyleSheet.create({
    wrapper: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'row',
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 30,
        marginLeft: 15
    },
    circle: {
        height: 20,
        width: 20,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#ACACAC',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 5
    },
    checkedCircle: {
        width: 14,
        height: 14,
        borderRadius: 7,
        backgroundColor: '#794F9B',
    },
});
