import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import HomeScreen from './screens/HomeScreen';
import ImageScreen from './screens/ImageScreen';

const MainNavigator = createStackNavigator(
  {
    Home: { screen: HomeScreen },
    Image: { screen: ImageScreen },
  },
  {
    headerLayoutPreset: 'center'
  }
);

const App = createAppContainer(MainNavigator);

export default App;
